PROMPT Creating user HOTEL_MANAGER...

--kiegészítés
alter session set "_ORACLE_SCRIPT"=true; 

DECLARE 
    CURSOR cur IS 
      SELECT 'alter system kill session ''' 
             || sid 
             || ',' 
             || serial# 
             || '''' AS command 
      FROM   v$session 
      WHERE  username = 'HOTEL_MANAGER'; 
BEGIN 
    FOR c IN cur LOOP 
        EXECUTE IMMEDIATE c.command; 
    END LOOP; 
END; 
/ 

DECLARE
  v_count NUMBER;
BEGIN
  SELECT COUNT(*) INTO v_count FROM dba_users t WHERE t.username='HOTEL_MANAGER';
  IF v_count = 1 THEN 
    EXECUTE IMMEDIATE 'DROP USER hotel_manager CASCADE';
  END IF;
END;
/
CREATE USER hotel_manager 
  IDENTIFIED BY "12345678" 
  DEFAULT TABLESPACE users
  QUOTA UNLIMITED ON users
;

GRANT CREATE SESSION TO hotel_manager;
GRANT CREATE TABLE TO hotel_manager;
GRANT CREATE VIEW TO hotel_manager;
GRANT CREATE TRIGGER TO hotel_manager;
GRANT CREATE SEQUENCE TO hotel_manager;
GRANT CREATE PROCEDURE TO hotel_manager;
grant create type to hotel_manager;

ALTER SESSION SET CURRENT_SCHEMA=hotel_manager;

PROMPT Done.

