create view vw_reservation_details as
select r.id as reservation_id,
       g.id as guest_id,
       g.first_name || ' ' || g.last_name as guest_name,
       r.arrival_date || ' - ' || r.leaving_date as stay,
       h.hotel_name,
       p.pack_name,
       ro.room_number
  from guest g
  join reservation r
    on g.id = r.guest_id
  join room ro
    on r.room_id = ro.id
  join pack p
    on p.id = ro.pack_id
  join hotel h
    on h.id = p.hotel_id;
    
