create view vw_hotel_details as
select h.id as hotel_id,
       h.hotel_name,
       p.id as pack_id,
       p.pack_name,
       s.id as service_id,
       s.service_name,
       s.service_desription,
       e.employee_id,
       e.first_name || ' ' || e.last_name as full_name,
       e.payment
  from hotel h
  join pack p
    on h.id = p.hotel_id
  join service s
    on s.pack_id = p.id
  join employee e
    on e.service_id = s.id;
