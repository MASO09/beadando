create view vw_hotel_room_details as
select h.id as hotel_id,
       h.hotel_name,
       p.id as pack_id,
       p.pack_name,
       r.id as room_id,
       r.room_number,
       r.already_reserved
 from hotel h
  join pack p
    on h.id = p.hotel_id
 join room r
    on r.pack_id = p.id;
