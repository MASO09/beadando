PROMPT Loading Rooms...

insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (1, 123, 1, 1, 12000, 0);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (2, 124, 1, 1, 10000, 0);

insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (3, 125, 1, 1, 10000, 0);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (4, 126, 1, 1, 15000, 1);
  
  insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (5, 213, 2, 1, 12000, 0);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (6, 214, 2, 1, 11000, 1);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (7, 215, 2, 2, 15000, 1);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (8, 216, 2, 2, 14000, 0);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (9, 217, 2, 1, 16000, 1);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (10, 311, 3, 1, 10000, 0);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (11, 312, 3, 2, 12000, 1);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (12, 313, 3, 2, 12000, 0);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (13, 314, 3, 1, 19000, 1);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (14, 315, 3, 2, 20000, 0);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (15, 412, 4, 1, 8000, 0);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (16, 413, 4, 1, 8000, 0);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (17, 414, 4, 1, 9000, 1);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (18, 512, 5, 2, 13000, 0);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (19, 513, 5, 2, 14000, 1);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (20, 514, 5, 2, 14000, 0);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (21, 615, 6, 1, 16000, 1);

insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (22, 616, 6, 1, 16000, 0);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (23, 617, 6, 2, 18000, 1);

insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (24, 715, 7, 2, 12000, 0);

insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (25, 716, 7, 2, 14000, 0);

insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (26, 812, 8, 1, 12000, 0);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (27, 813, 8, 2, 15000, 1);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (28, 814, 8, 1, 14000, 0);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (29, 911, 9, 1, 15000, 1);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (30, 912, 9, 2, 16000, 0);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (31, 913, 9, 2, 15000, 0);

insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (32, 1017, 10, 1, 10000, 0);

insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (33, 1018, 10, 1, 11000, 1);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (34, 1019, 10, 2, 12000, 0);

insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (35, 1117, 11, 1, 11000, 0);

insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (36, 1118, 11, 2, 12000, 1);

insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (37, 1211, 12, 2, 12000, 1);
  
insert into room
  (id, room_number, pack_id, number_of_beds, price, already_reserved)
values
  (38, 1218, 12, 2, 12000, 1);
  
commit;


  
  
  
  
  
  
  
  
  
