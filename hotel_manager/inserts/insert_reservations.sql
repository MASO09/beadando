PROMPT Loading Reservations...

insert into reservation
       (id, guest_id, room_id, arrival_date, leaving_date)
values
       (1, 1, 4, to_date('2022-01-09', 'yyyy-mm-dd'),  to_date('2022-01-11', 'yyyy-mm-dd'));
       
insert into reservation
       (id, guest_id, room_id, arrival_date, leaving_date)
values
       (2, 2, 13, to_date('2022-01-09', 'yyyy-mm-dd'),  to_date('2022-01-11', 'yyyy-mm-dd'));
       
insert into reservation
       (id, guest_id, room_id, arrival_date, leaving_date)
values
       (3, 3, 19, to_date('2022-01-09', 'yyyy-mm-dd'),  to_date('2022-01-11', 'yyyy-mm-dd'));
       
insert into reservation
       (id, guest_id, room_id, arrival_date, leaving_date)
values
       (4, 4, 23, to_date('2022-01-09', 'yyyy-mm-dd'),  to_date('2022-01-11', 'yyyy-mm-dd'));
       

commit;
