PROMPT Loading Employees...

insert into employee
  (employee_id, service_id, first_name, last_name, email, tel, payment, birth_date)
values
  (1,2,'S�ndor','Nagy', 'temp1@gmail.com','20/1234567',200000,to_date('2000-07-09', 'yyyy-mm-dd'));

insert into employee
  (employee_id, service_id, first_name, last_name, email, tel, payment, birth_date)
values
  (2,3,'Bence', 'Juh�sz', 'temp2@gmail.com','20/2234567',200000,to_date('1998-04-09', 'yyyy-mm-dd'));

insert into employee
  (employee_id, service_id, first_name, last_name, email, tel, payment, birth_date)
values
  (3,5,'Judit', 'Lov�sz', 'temp3@gmail.com','30/4234567',240000,to_date('1988-09-19', 'yyyy-mm-dd'));

insert into employee
  (employee_id, service_id, first_name, last_name, email, tel, payment, birth_date)
values
  (4,6,'�d�m', 'Kerekes', 'temp4@gmail.com','23/2234567',260000,to_date('1994-04-09', 'yyyy-mm-dd'));

insert into employee
  (employee_id, service_id, first_name, last_name, email, tel, payment, birth_date)
values
  (5,8,'Krisztina', 'Varga', 'temp5@gmail.com','30/2234567',200000,to_date('1999-08-21', 'yyyy-mm-dd'));

insert into employee
  (employee_id, service_id, first_name, last_name, email, tel, payment, birth_date)
values
  (6,9,'Tibor', 'Szerecsen', 'temp6@gmail.com','32/6734567',290000,to_date('1990-08-21', 'yyyy-mm-dd'));

insert into employee
  (employee_id, service_id, first_name, last_name, email, tel, payment, birth_date)
values
  (7,11,'Kl�ra', 'Heged�s', 'temp7@gmail.com','12/6734567',230000,to_date('1992-05-11', 'yyyy-mm-dd'));
  
insert into employee
  (employee_id, service_id, first_name, last_name, email, tel, payment, birth_date)
values
  (8,12,'Viktor', 'Leveles', 'temp8@gmail.com','32/4324567',300000,to_date('1985-08-13', 'yyyy-mm-dd'));

commit;

