


-- Esetleg instlall logol�s
SET serveroutput ON
spool .\install.log

PROMPT Installing DB...
-- Install sequences
PROMPT installing SEQUENCES...
@./sequence/employee_id_seq.sql
@./sequence/guest_id_seq.sql
@./sequence/hotel_id_seq.sql
@./sequence/pack_id_seq.sql
@./sequence/reservation_id_seq.sql
@./sequence/room_id_seq.sql
@./sequence/service_id_seq.sql

-- Install tables
PROMPT installing TABLES...

@./tables/employee_table.sql
@./tables/guest_table.sql
@./tables/reservation_table.sql
@./tables/room_table.sql
@./tables/pack_table.sql
@./tables/hotel_table.sql
@./tables/service_table.sql

@./tables/employee_table_h.sql
@./tables/guest_table_h.sql
@./tables/reservation_table_h.sql
@./tables/room_table_h.sql
@./tables/pack_table_h.sql
@./tables/hotel_table_h.sql
@./tables/service_table_h.sql


-- Install types
PROMPT installig TYPES...
@./type/ty_hotel_packs_rooms.typ
@./type/ty_employee_details.typ
@./type/ty_hotel.typ
@./type/ty_reservation.typ

@./type/ty_hotel_array.typ
@./type/ty_employee_array.typ
@./type/ty_reservation_array.typ
@./type/ty_array_hotel_room.typ

--Procedures
PROMPT installing PROCEDURES...
@./procedure/add_employee.prc
@./procedure/add_guest.prc
@./procedure/add_hotel.prc
@./procedure/add_pack.prc
@./procedure/add_room.prc
@./procedure/add_service.prc

-- Views
PROMPT installing VIEWS...
@./view/vw_hotel_details.sql
@./view/vw_hotel_room_details.sql
@./view/vw_reservation_details.sql

-- Packes
PROMPT installing PACKAGES...
@./package/pkg_get_data.pck
@./package/pkg_reservation_management.pck



-- Triggers
PROMPT installing TRIGGERS...

@./trigger/employee_seq_trg.pdc
@./trigger/guest_seq_trg.pdc
@./trigger/hotel_seq_trg.pdc
@./trigger/pack_seq_trg.pdc
@./trigger/reservation_seq_trg.pdc
@./trigger/room_seq_trg.pdc
@./trigger/service_seq_trg.pdc

@./trigger/employee_trg.pdc
@./trigger/guest_trg.pdc
@./trigger/hotel_trg.pdc
@./trigger/pack_trg.pdc
@./trigger/reservation_trg.pdc
@./trigger/reservation_price_trg.pdc
@./trigger/room_trg.pdc
@./trigger/service_trg.pdc

-- Alter
-- PK
PROMPT installing ALTERS...
@./alter/pk_employee.sql
@./alter/pk_guest.sql
@./alter/pk_hotel.sql
@./alter/pk_pack.sql
@./alter/pk_reservation.sql
@./alter/pk_room.sql
@./alter/pk_service.sql
-- FK
@./alter/fk_employee.sql
@./alter/fk_pack.sql
@./alter/fk_reservation.sql
@./alter/fk_reservation_room.sql
@./alter/fk_room.sql
@./alter/fk_service.sql
commit;


-- Recompile schema
BEGIN
  dbms_utility.compile_schema(schema => 'HOTEL_MANAGER');
END;
/

-- Tabla data
PROMPT loading DATA...
@./inserts/insert_guests.sql
@./inserts/insert_hotels.sql
@./inserts/insert_packs.sql
@./inserts/insert_rooms.sql
@./inserts/insert_reservations.sql
@./inserts/insert_services.sql
@./inserts/insert_employees.sql
PROMPT Done.

spool off
