--------------------------------------
--Test pkg_get_data.get_free_rooms_by_num
--------------------------------------
declare
  -- Non-scalar parameters require additional processing 
  result ty_array_hotel_room;
begin
  -- Call the function
  result := pkg_get_data.get_free_rooms_by_hotel_id(p_hotel_id => 2);
  for i in 1 .. result.count loop
    dbms_output.put_line(result(i)
                         .var_room_number || ' ' || result(i).var_pack_name);
  end loop;
end;

--------------------------------------
--Test pkg_get_data.get_free_rooms_by_name
--------------------------------------
declare
  -- Non-scalar parameters require additional processing 
  result ty_array_hotel_room;
begin
  -- Call the function
  result := pkg_get_data.get_free_rooms_by_hotel_name(p_hotel_name => 'Amour');
  for i in 1 .. result.count loop
    dbms_output.put_line(result(i).var_room_number);
  end loop;
end;

--------------------------------------
--Test pkg_get_data.get_hotels
--------------------------------------
declare
  -- Non-scalar parameters require additional processing 
  result ty_hotel_array;
begin
  -- Call the function
  result := pkg_get_data.get_hotels;
  for i in 1 .. result.count loop
    dbms_output.put_line(result(i)
                         .p_hotel_name || ', ' || result(i).p_country);
  end loop;
end;

--------------------------------------
--Test pkg_get_data.get_employees function
--------------------------------------
declare
  -- Non-scalar parameters require additional processing 
  result ty_employee_array;
begin
  -- Call the function
  result := pkg_get_data.get_employees;
  for i in 1 .. result.count loop
    dbms_output.put_line(result(i).p_employee_id || ' ' || result(i).p_name || ' ' || result(i)
                         .p_payment);
  end loop;
end;

--------------------------------------
--Test pkg_get_data.get_reservation
--------------------------------------

declare
  -- Non-scalar parameters require additional processing 
  result ty_reservation_array;
begin
  -- Call the function
  result := pkg_get_data.get_reservation(p_reservation_id => 2);
  for i in 1 .. result.count loop
    dbms_output.put_line(result(i).p_guest_name || ', ' || result(i).p_stay || ', ' || result(i)
                         .p_hotel);
  end loop;
end;

--------------------------------------
--Test pkg_reservation_management.add_reservation
--------------------------------------
begin
  -- Call the procedure
  pkg_reservation_management.add_reservation(p_guest_id     => 2,
                                             p_room_id      => 1,
                                             p_arrival_date => '2022-01-09',
                                             p_leaving_date => '2022-01-11');
end;

select * from reservation r where r.guest_id = 2;

--------------------------------------
--Test pkg_reservation_management.delete_reservation
--------------------------------------

begin
  -- Call the procedure
  pkg_reservation_management.delete_reservation(p_reservation_id => 1);
end;

select * from reservation r where r.guest_id = 1;

--------------------------------------
--Test add procedures
--------------------------------------

begin
  -- Call the procedure
  add_employee(p_service_id => 2,
               p_first_name => 'Bob',
               p_last_name  => 'Balogh',
               p_email      => 'test@xymail.com',
               p_tel        => '12/9753124',
               p_payment    => 300000,
               p_birth_date => '1991-05-14');
end;

begin

  add_guest(p_first_name => 'Alice',
            p_last_name  => 'Schmidt',
            p_email      => 'temp01@xymail.com',
            p_tel        => '21/3456435');
end;

begin
  -- Call the procedure
  add_hotel(p_hotel_name => 'Sz�p Sz�ll�',
            p_rating => 4,
            p_country => 'Magyarorsz�g',
            p_city => 'P�cs',
            p_street => 'Titok utca 3.');
end;

begin
  -- Call the procedure
  add_pack(p_hotel_id => 2,
           p_pack_name => '�j pack',
           p_price => 10000);
end;

begin
  -- Call the procedure
  add_room(p_room_number => 0117,
           p_pack_id => 3,
           p_number_of_beds => 2,
           p_price => 15000);
end;

begin
  -- Call the procedure
  add_service(p_pack_id => 3,
              p_service_name => '�j szolg�ltat�s',
              p_service_desription => 'Itt a helye a le�r�snak.');
end;



