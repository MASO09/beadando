create or replace type ty_hotel as object
(
  p_hotel_name varchar2(40),
  p_country varchar2(40),
  p_city varchar2(40),
  p_rating number
)
/
