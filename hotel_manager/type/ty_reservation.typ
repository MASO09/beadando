create or replace type ty_reservation as object
(
    p_guest_id    number,
    p_guest_name  varchar2(85),
    p_stay        varchar2(30),
    p_hotel       varchar2(40),
    p_pack        varchar2(30)
)
/
