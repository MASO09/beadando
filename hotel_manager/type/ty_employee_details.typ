create or replace type ty_employee_details as object
(
  p_employee_id number,
  p_name varchar2(85),
  p_payment number,
  p_service_name varchar2(200),
  p_hotel_name varchar2(40)
)
/
