create or replace procedure add_guest(p_first_name varchar2,
                                      p_last_name  varchar2,
                                      p_email      varchar2,
                                      p_tel        varchar2) is
begin
  insert into guest
    (first_name, last_name, email, tel)
  values
    (p_first_name, p_last_name, p_email, p_tel);

end add_guest;
/
