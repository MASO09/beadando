create or replace procedure add_hotel(p_hotel_name varchar2,
                                      p_rating     number,
                                      p_country    varchar2,
                                      p_city       varchar2,
                                      p_street     varchar2) is
begin
  insert into hotel
    (hotel_name, rating, country, city, street)
  values
    (p_hotel_name, p_rating, p_country, p_city, p_street);
end add_hotel;
/
