create or replace procedure add_room(p_room_number    number,
                                     p_pack_id        number,
                                     p_number_of_beds number,
                                     p_price          number) is
  p_room_exists_in_pack number;
  room_exists_exc exception;
  pragma exception_init(room_exists_exc, -20001);
begin
  select count(*)
    into p_room_exists_in_pack
    from room r
    where r.pack_id = p_pack_id
     and r.room_number = p_room_number;

  if p_room_exists_in_pack >= 1 then
     raise_application_error(-20001, 'M�r van ilyen szoba a hotelben.');
  else
  
    insert into room
      (room_number, pack_id, number_of_beds, price, already_reserved)
    values
      (p_room_number, p_pack_id, p_number_of_beds, p_price, 0);
  end if;
end add_room;
/
