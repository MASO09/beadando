create or replace procedure add_service(p_pack_id            number,
                                        p_service_name       varchar2,
                                        p_service_desription varchar2) is
begin
  insert into service
    (pack_id, service_name, service_desription)
  values
    (p_pack_id, p_service_name, p_service_desription);
end add_service;
/
