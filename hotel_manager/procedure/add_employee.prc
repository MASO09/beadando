create or replace procedure add_employee(p_service_id  number,
                                         p_first_name  varchar2,
                                         p_last_name   varchar2,
                                         p_email       varchar2,
                                         p_tel         varchar2,
                                         p_payment     varchar2,
                                         p_birth_date  varchar2) is
begin
  insert into employee
    (service_id, first_name, last_name, email, tel, payment, birth_date)
  values
    (p_service_id,
     p_first_name,
     p_last_name,
     p_email,
     p_tel,
     p_payment,
     to_date(p_birth_date, 'YYYY-MM-DD'));
end add_employee;
/
