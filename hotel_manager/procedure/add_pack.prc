create or replace procedure add_pack(p_hotel_id number, p_pack_name varchar2, p_price number) is
begin
  insert into pack(hotel_id,
                   pack_name,
                   price)
                   values(
                   p_hotel_id,
                   p_pack_name,
                   p_price
                   );
end add_pack;
/
