PROMPT creating TABLE pack...
create table pack(
       ID number not null,
       hotel_id number not null,
       pack_name varchar2(30) not null,
       price number not null,
       last_modified DATE DEFAULT SYSDATE NOT NULL,
       created DATE DEFAULT SYSDATE NOT NULL,
       mod_user VARCHAR2(300),
       dml_flag     VARCHAR2(1),
       version      NUMBER
)TABLESPACE users;
