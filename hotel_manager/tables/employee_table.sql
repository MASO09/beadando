PROMPT creating TABLE employee...
create table employee(
       employee_id number not null,
       service_id number not null,
       first_name VARCHAR2(40) NOT NULL,
       last_name VARCHAR2(40) NOT NULL,
       email VARCHAR2(100) not null,
       tel VARCHAR2(20) not null,
       payment number not null,
       birth_date date not null,
       last_modified DATE DEFAULT SYSDATE NOT NULL,
       created DATE DEFAULT SYSDATE NOT NULL,
       mod_user VARCHAR2(300),
       dml_flag     VARCHAR2(1),
       version      NUMBER
)TABLESPACE users;
