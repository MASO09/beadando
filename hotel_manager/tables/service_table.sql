PROMPT creating TABLE service...
create table service(
       ID number not null,
       pack_id number not null,
       service_name varchar2(200) not null,
       service_desription varchar2(400),
       last_modified DATE DEFAULT SYSDATE NOT NULL,
       created DATE DEFAULT SYSDATE NOT NULL,
       mod_user VARCHAR2(300),
       dml_flag     VARCHAR2(1),
       version      NUMBER
)TABLESPACE users;
