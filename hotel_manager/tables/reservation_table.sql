PROMPT creating TABLE resevation...
create table reservation(
       ID number not null,
       guest_id number not null,
       room_id number not null,
       arrival_date date not null,
       leaving_date date not null,
       price number not null,
       last_modified DATE DEFAULT SYSDATE NOT NULL,
       created DATE DEFAULT SYSDATE NOT NULL,
       mod_user VARCHAR2(300),
       dml_flag     VARCHAR2(1),
       version      NUMBER 
)TABLESPACE users;
--erre kell egy trigger ami kisz�molja napok �s csomag alapj�n a dolgokat
