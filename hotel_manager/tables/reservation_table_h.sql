PROMPT creating TABLE reservation_h...
create table reservation_h(        
       ID number,
       guest_id number,
       room_id number ,
       arrival_date date,
       leaving_date date,
       price number,
       last_modified DATE,
       created DATE,
       mod_user VARCHAR2(300),
       dml_flag     VARCHAR2(1),
       version      NUMBER 
)TABLESPACE users;
