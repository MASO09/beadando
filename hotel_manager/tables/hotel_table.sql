PROMPT ceating TABLE hotel...
create table hotel(
       ID number not null,
       hotel_name varchar2(40) not null,
       rating number not null,
       country varchar(40) not null,
       city varchar(40) not null,
       street varchar(200) not null,
       last_modified DATE DEFAULT SYSDATE NOT NULL,
       created DATE DEFAULT SYSDATE NOT NULL,
       mod_user VARCHAR2(300),
       dml_flag     VARCHAR2(1),
       version      NUMBER
)TABLESPACE users;
