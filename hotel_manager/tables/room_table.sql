PROMPT creating TABLE room...
create table room(
       ID number not null,
       room_number number not null,
       pack_id number not null,
       number_of_beds number not null,
       price number not null,
       already_reserved number not null,
       last_modified DATE DEFAULT SYSDATE NOT NULL,
       created DATE DEFAULT SYSDATE NOT NULL,
       mod_user VARCHAR2(300),
       dml_flag     VARCHAR2(1),
       version      NUMBER
)TABLESPACE users;
