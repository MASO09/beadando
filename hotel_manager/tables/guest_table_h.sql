PROMPT creating TABLE users...
create table guest_h(
       ID NUMBER,
       first_name VARCHAR2(40) ,
       last_name VARCHAR2(40),
       email VARCHAR2(100),
       tel VARCHAR2(20),
       last_modified DATE,
       created DATE,
       mod_user VARCHAR2(300),
       dml_flag     VARCHAR2(1),
       version      NUMBER
)TABLESPACE users;
