PROMPT creating TABLE guest...
create table guest(
       ID NUMBER NOT NULL,
       first_name VARCHAR2(40) NOT NULL,
       last_name VARCHAR2(40) NOT NULL,
       email VARCHAR2(100) not null,
       tel VARCHAR2(20) not null,
       last_modified DATE DEFAULT SYSDATE NOT NULL,
       created DATE DEFAULT SYSDATE NOT NULL,
       mod_user VARCHAR2(300),
       dml_flag     VARCHAR2(1),
       version      NUMBER
)TABLESPACE users;
