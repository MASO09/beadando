PROMPT creating TABLE pack_h...
create table pack_h(
       ID number,
       hotel_id number,
       pack_name varchar2(30),
       price number,
       last_modified DATE,
       created DATE,
       mod_user VARCHAR2(300),
       dml_flag     VARCHAR2(1),
       version      NUMBER
)TABLESPACE users;
