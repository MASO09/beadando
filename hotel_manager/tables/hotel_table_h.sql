PROMPT crating TABLE hotel_h...
create table hotel_h(   
       ID number,
       hotel_name varchar2(40),
       rating number,
       country varchar(40),
       city varchar(40),
       street varchar(200),
       last_modified DATE ,
       created DATE,
       mod_user VARCHAR2(300),
       dml_flag     VARCHAR2(1),
       version      NUMBER
)TABLESPACE users;
