PROMPT creating TABLE service_h...
create table service_h(
       ID number,
       pack_id number,
       service_name varchar2(200),
       service_desription varchar2(400),
       last_modified DATE,
       created DATE,
       mod_user VARCHAR2(300),
       dml_flag     VARCHAR2(1),
       version      NUMBER
)TABLESPACE users;
