PROMPT creating TABLE room_h...
create table room_h(
  ID number not null,
       room_number number,
       pack_id number,
       number_of_beds number,
       price number,
       already_reserved number,
       last_modified DATE,
       created DATE,
       mod_user VARCHAR2(300),
       dml_flag     VARCHAR2(1),
       version      NUMBER
)TABLESPACE users;
