PROMPT creating TABLE employee_h...
create table employee_h(
       employee_id number,
       service_id number,
       first_name VARCHAR2(40),
       last_name VARCHAR2(40),
       email VARCHAR2(100),
       tel VARCHAR2(20),
       payment number,
       birth_date date,
       last_modified DATE,
       created DATE,
       mod_user VARCHAR2(300),
       dml_flag     VARCHAR2(1),
       version      NUMBER
)TABLESPACE users;
