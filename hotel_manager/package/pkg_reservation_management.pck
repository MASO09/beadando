create or replace package pkg_reservation_management is

  -- Author  : MASO
  -- Created : 2022. 01. 03. 15:25:26
  -- Purpose : 

  procedure add_reservation(p_guest_id     number,
                            p_room_id      number,
                            p_arrival_date varchar2,
                            p_leaving_date varchar2);

  procedure delete_reservation(p_reservation_id number);

end pkg_reservation_management;
/
create or replace package body pkg_reservation_management is

  procedure add_reservation(p_guest_id     number,
                            p_room_id      number,
                            p_arrival_date varchar2,
                            p_leaving_date varchar2) is
    p_reserved number;
    room_is_reserved_exc exception;
    pragma exception_init(room_is_reserved_exc, -20000);
  begin
    select r.already_reserved
      into p_reserved
      from room r
     where r.id = p_room_id;
  
    if p_reserved = 1 then
      raise_application_error(-20000, 'A szoba m�r foglalt.');
    
    else
      insert into reservation
        (guest_id, room_id, arrival_date, leaving_date)
      values
        (p_guest_id,
         p_room_id,
         to_date(p_arrival_date, 'YYYY-MM_DD'),
         to_date(p_leaving_date, 'YYYY-MM-DD'));
    
    end if;
  
  end add_reservation;

  procedure delete_reservation(p_reservation_id number) is
    no_reservation_found_exc exception;
    pragma exception_init(no_reservation_found_exc, -20002);
    p_room_id number;
  begin
  
    select re.room_id
      into p_room_id
      from reservation re
     where re.id = p_reservation_id;
  
    update room ro set ro.already_reserved = 0 where ro.id = p_room_id;
  
    delete from reservation r where r.id = p_reservation_id;
  
  exception
    when no_data_found then
      raise_application_error(-20002,
                              'Nincs ilyen id-vel rendelkez� foglal�s.');
    
  end delete_reservation;

end pkg_reservation_management;
/
