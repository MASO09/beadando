create or replace package pkg_get_data is

  
  function get_reservation(p_reservation_id number)
    return ty_reservation_array;

  function get_free_rooms_by_hotel_id(p_hotel_id in number)
    return ty_array_hotel_room;

  function get_free_rooms_by_hotel_name(p_hotel_name in varchar2)
    return ty_array_hotel_room;

  function get_employees return ty_employee_array;
  
  function get_hotels return ty_hotel_array;
end pkg_get_data;
/
create or replace package body pkg_get_data is

  function get_reservation(p_reservation_id number)
    return ty_reservation_array is
    res_array ty_reservation_array;
  begin
    res_array := ty_reservation_array();
    for i in (select v.guest_id,
                     v.guest_name,
                     v.stay,
                     v.hotel_name,
                     v.pack_name
                from vw_reservation_details v
               where v.reservation_id = p_reservation_id) loop
      res_array.extend();
      res_array(res_array.count) := ty_reservation(p_guest_id   => i.guest_id,
                                                   p_guest_name => i.guest_name,
                                                   p_stay       => i.stay,
                                                   p_hotel      => i.hotel_name,
                                                   p_pack       => i.pack_name);
    end loop;
    return res_array;
  end;

  function get_free_rooms_by_hotel_id(p_hotel_id in number)
    return ty_array_hotel_room is
    hpr_array ty_array_hotel_room;
  begin
    hpr_array := ty_array_hotel_room();
  
    for i in (select v.pack_name, v.room_number
                from vw_hotel_room_details v
               where v.hotel_id = p_hotel_id
                 and v.already_reserved = 0) loop
      hpr_array.extend();
      hpr_array(hpr_array.count) := ty_hotel_packs_rooms(var_pack_name   => i.pack_name,
                                                         var_room_number => i.room_number);
    end loop;
    return hpr_array;
  
  end;

  function get_free_rooms_by_hotel_name(p_hotel_name in varchar2)
    return ty_array_hotel_room is
    hpr_array ty_array_hotel_room;
  begin
    hpr_array := ty_array_hotel_room();
    for i in (select v.pack_name, v.room_number
                from vw_hotel_room_details v
               where v.hotel_name like '%' || p_hotel_name || '%'
                 and v.already_reserved = 0) loop
      hpr_array.extend();
      hpr_array(hpr_array.count) := ty_hotel_packs_rooms(var_pack_name   => i.pack_name,
                                                         var_room_number => i.room_number);
    end loop;
    return hpr_array;
  end;

  function get_employees return ty_employee_array is
    employee_array ty_employee_array;
  begin
    employee_array := ty_employee_array();
  
    for i in (select v.hotel_name,
                     v.service_name,
                     v.employee_id,
                     v.full_name,
                     v.payment
                from vw_hotel_details v) loop
      employee_array.extend();
      employee_array(employee_array.count) := ty_employee_details(p_employee_id  => i.employee_id,
                                                                  p_name         => i.full_name,
                                                                  p_payment      => i.payment,
                                                                  p_service_name => i.service_name,
                                                                  p_hotel_name   => i.hotel_name);
    end loop;
    return employee_array;
  end;

  function get_hotels return ty_hotel_array is
           h_array ty_hotel_array;
    begin
      h_array := ty_hotel_array();
      for i in (
        select h.hotel_name, h.rating, h.country, h.city
        from hotel h
        )
        loop
          h_array.extend();
          h_array(h_array.count) := ty_hotel(
                                        p_hotel_name => i.hotel_name,
                                        p_country => i.country,
                                        p_city => i.city,
                                        p_rating => i.rating);
        end loop;
        return h_array;
    end;
end pkg_get_data;
/
